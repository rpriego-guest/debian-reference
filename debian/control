Source: debian-reference
Section: doc
Priority: optional
Maintainer: Osamu Aoki <osamu@debian.org>
Build-Depends: debhelper (>= 11)
Build-Depends-Indep: dblatex,
                     docbook-xml,
                     docbook-xsl,
                     libopencc2-data,
                     fonts-liberation2,
                     fonts-vlgothic,
                     fonts-wqy-microhei,
                     libxml2-utils,
                     locales-all | locales,
                     opencc,
                     po4a,
                     python,
                     texlive-lang-chinese,
                     texlive-lang-english,
                     texlive-lang-french,
                     texlive-lang-german,
                     texlive-lang-italian,
                     texlive-lang-japanese,
                     texlive-lang-portuguese,
                     texlive-lang-spanish,
                     texlive-xetex,
                     w3m,
                     xsltproc,
                     zip
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/debian/debian-reference.git
Vcs-Browser: https://salsa.debian.org/debian/debian-reference/
Homepage: https://www.debian.org/doc/user-manuals#quick-reference

Package: debian-reference-common
Architecture: all
Depends: ${misc:Depends}
Suggests: calibre, mc, vim, w3m | www-browser
Description: Debian system administration guide, common files
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 This provides menu functionality and other common files.

Package: debian-reference
Architecture: all
Depends: debian-reference-en, ${misc:Depends}
Recommends: debian-reference-fr,
            debian-reference-it,
            debian-reference-ja,
            debian-reference-es,
            debian-reference-de,
            debian-reference-pt,
            debian-reference-zh-cn,
            debian-reference-zh-tw
Description: metapackage to install (all) translations of Debian Reference
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 This installs all translations when "Recommends:" are installed.

Package: debian-reference-en
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: doc-base
Description: Debian system administration guide, English original
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The English original version.

Package: debian-reference-de
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, German translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The German translation.

Package: debian-reference-fr
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, French translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The French translation.

Package: debian-reference-it
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Italian translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Italian translation.

Package: debian-reference-ja
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Japanese translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Japanese translation.

Package: debian-reference-pt
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Portuguese translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Portuguese (Portugal) translation.

Package: debian-reference-zh-cn
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Chinese (Simplified) translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Chinese (Simplified) translation.

Package: debian-reference-zh-tw
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Chinese (Traditional) translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Chinese (Traditional) translation.

Package: debian-reference-es
Architecture: all
Depends: debian-reference-common (>= 2.51), ${misc:Depends}
Suggests: debian-reference-en, doc-base
Description: Debian system administration guide, Spanish translation
 This Debian Reference is intended to provide a broad overview of the Debian
 system as a post-installation user's guide. It covers many aspects of system
 administration through shell-command examples for non-developers.
 .
 The Spanish translation.
